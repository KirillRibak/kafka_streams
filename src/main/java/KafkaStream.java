import model.Hotel;
import model.HotelWithWeather;
import model.Weather;
import model.json.JsonPOJODeserializer;
import model.json.JsonPOJOSerializer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.StreamsConfig;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

public abstract class KafkaStream {
    public static final String STREAM_TOPIC = "weather";
    public static final String TABLE_TOPIC = "hotels";
    public static final String OUTPUT_TOPIC = "hotel-with-wthr";
    public static final String SMALL_TABLE_TOPIC = "smallKeyHotel";

    private Map<String, Object> serdeProps;
    public Deserializer<Weather> weatherDeserializer;
    public Serializer<Weather> weatherSerializer;
    public Deserializer<Hotel> hotelDeserializer;
    public Serializer<Hotel> hotelSerializer;
    private Serde<Weather> weatherSerde;
    private Serde<Hotel> hotelSerde;
    public Deserializer<HotelWithWeather> hotelWithWeatherDeserializer;
    public Serializer<HotelWithWeather> hotelWithWeatherSerializer;
    private Serde<HotelWithWeather> hotelWithWeatherSerde;


    public KafkaStream() {
        serdeProps = new HashMap<>();
        serdeProps.put("JsonPOJOClass", Weather.class);
        weatherDeserializer = new JsonPOJODeserializer<>();
        weatherDeserializer.configure(serdeProps, false);
        weatherSerializer = new JsonPOJOSerializer<>();
        weatherSerializer.configure(serdeProps, false);
        weatherSerde = Serdes.serdeFrom(weatherSerializer, weatherDeserializer);
        serdeProps.put("JsonPOJOClass", Hotel.class);
        hotelDeserializer = new JsonPOJODeserializer<>();
        hotelDeserializer.configure(serdeProps, false);
        hotelSerializer = new JsonPOJOSerializer<>();
        hotelSerializer.configure(serdeProps, false);
        hotelSerde = Serdes.serdeFrom(hotelSerializer, hotelDeserializer);
        serdeProps.put("JsonPOJOClass", HotelWithWeather.class);
        hotelWithWeatherDeserializer = new JsonPOJODeserializer<>();
        hotelWithWeatherDeserializer.configure(serdeProps, false);
        hotelWithWeatherSerializer = new JsonPOJOSerializer<>();
        hotelWithWeatherSerializer.configure(serdeProps, false);
        hotelWithWeatherSerde
                = Serdes.serdeFrom(hotelWithWeatherSerializer, hotelWithWeatherDeserializer);
    }

    public Serde<Weather> getWeatherSerde() {
        return weatherSerde;
    }

    public Serde<Hotel> getHotelSerde() {
        return hotelSerde;
    }

    public Serde<HotelWithWeather> getHotelWithWeatherSerde() {
        return hotelWithWeatherSerde;
    }

    public Properties getProperties() {
        Properties properties = new Properties();
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, UUID.randomUUID().toString());
        properties.put(ConsumerConfig.CLIENT_ID_CONFIG, UUID.randomUUID().toString());
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, UUID.randomUUID().toString());
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "sandbox-hdp.hortonworks.com:6667");
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return properties;
    }

}
