package model;

public class HotelWithWeather {
    private String id;
    private String name;
    private String country;
    private String city;
    private String address;
    private String latitude;
    private String longitude;
    private String avg_tmpr_f;
    private String avg_tmpr_c;
    private String wthr_date;
    private int precision;

    public HotelWithWeather() {
    }

    public HotelWithWeather(Weather weather, Hotel hotel) {
        if (hotel != null) {
            id = hotel.getId();
            name = hotel.getName();
            country = hotel.getCountry();
            city = hotel.getCity();
            address = hotel.getAddress();
            latitude = hotel.getLatitude();
            longitude = hotel.getLongitude();
        }
        if (weather != null) {
            avg_tmpr_f = weather.getAvg_tmpr_f();
            avg_tmpr_c = weather.getAvg_tmpr_c();
            wthr_date = weather.getWthr_date();
        }
    }

    public int getPrecision() {
        return precision;
    }

    public HotelWithWeather setPrecision(int precision) {
        this.precision = precision;
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAvg_tmpr_f() {
        return avg_tmpr_f;
    }

    public void setAvg_tmpr_f(String avg_tmpr_f) {
        this.avg_tmpr_f = avg_tmpr_f;
    }

    public String getAvg_tmpr_c() {
        return avg_tmpr_c;
    }

    public void setAvg_tmpr_c(String avg_tmpr_c) {
        this.avg_tmpr_c = avg_tmpr_c;
    }

    public String getWthr_date() {
        return wthr_date;
    }

    public void setWthr_date(String wthr_date) {
        this.wthr_date = wthr_date;
    }

    public HotelWithWeather populateHotelData(Hotel hotel) {
        if (hotel != null) {
            id = hotel.getId();
            name = hotel.getName();
            country = hotel.getCountry();
            city = hotel.getCity();
            address = hotel.getAddress();
            latitude = hotel.getLatitude();
            longitude = hotel.getLongitude();
        }
        return this;
    }

    @Override
    public String toString() {
        return "HotelWithWeather{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", avg_tmpr_f=" + avg_tmpr_f +
                ", avg_tmpr_c=" + avg_tmpr_c +
                ", wthr_date='" + wthr_date + '\'' +
                ", precision=" + precision +
                '}';
    }
}
