package model;

public class Weather {
    private String lng;
    private String lat;
    private String avg_tmpr_f;
    private String avg_tmpr_c;
    private String wthr_date;

    public Weather() {
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getAvg_tmpr_f() {
        return avg_tmpr_f;
    }

    public void setAvg_tmpr_f(String avg_tmpr_f) {
        this.avg_tmpr_f = avg_tmpr_f;
    }

    public String getAvg_tmpr_c() {
        return avg_tmpr_c;
    }

    public void setAvg_tmpr_c(String avg_tmpr_c) {
        this.avg_tmpr_c = avg_tmpr_c;
    }

    public String getWthr_date() {
        return wthr_date;
    }

    public void setWthr_date(String wthr_date) {
        this.wthr_date = wthr_date;
    }

    @Override
    public String toString() {
        return "Weather{" +
                "lng=" + lng +
                ", lat=" + lat +
                ", avg_tmpr_d=" + avg_tmpr_f +
                ", avg_tmpr_c=" + avg_tmpr_c +
                ", wthr_date='" + wthr_date + '\'' +
                '}';
    }
}
