import ch.hsr.geohash.GeoHash;
import model.Hotel;
import model.HotelWithWeather;
import model.Weather;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.state.KeyValueStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KafkaStreamTask extends KafkaStream {

    private Logger log = LoggerFactory.getLogger(KafkaStream.class);


    public StreamsBuilder createHotelsWithWeather() {
        StreamsBuilder builder = new StreamsBuilder();
        KTable<String, Hotel> hotelsWithGeohash = builder.table(TABLE_TOPIC,
                Materialized.<String, Hotel, KeyValueStore<Bytes, byte[]>>as("HOTELS_GEOHASH_5_PRECISION")
                        .withKeySerde(Serdes.String()).withValueSerde(getHotelSerde()));
        KTable<String, Hotel> hotelsTableThreePrecision = builder.table(SMALL_TABLE_TOPIC,
                Materialized.<String, Hotel, KeyValueStore<Bytes, byte[]>>as("HOTELS_GEOHASH_3_PRECISION")
                        .withKeySerde(Serdes.String()).withValueSerde(getHotelSerde()));
        KStream<String, Weather> weather = builder.stream(STREAM_TOPIC, Consumed.with(Serdes.String(), getWeatherSerde()));
        KStream<String, Weather> weatherWithGeohash = weather.map((key, value) -> getChangedWeather(value, 5));
        KStream<String, HotelWithWeather> joinStream = weatherWithGeohash.leftJoin(hotelsWithGeohash,
                HotelWithWeather::new, Joined.with(Serdes.String(), getWeatherSerde(), getHotelSerde()));
        KStream<String, HotelWithWeather> recordsWithFivePrecision = joinStream.filterNot((k, v) -> v.getAddress() == null)
                .map((k, v) -> new KeyValue<>(k, v.setPrecision(5)));
        KStream<String, HotelWithWeather> nonJoinedValues = joinStream.filter((k, v) -> v.getAddress() == null)
                .map((k, v) -> new KeyValue<>(k.substring(0, 3), v));
        KStream<String, HotelWithWeather> recordsWithThreePrecision = nonJoinedValues.leftJoin(hotelsTableThreePrecision,
                (wValue, hValue) -> wValue.populateHotelData(hValue),
                Joined.with(Serdes.String(), getHotelWithWeatherSerde(), getHotelSerde()))
                .filterNot((k, v) -> v.getAddress() == null)
                .map((k, v) -> new KeyValue<>(k, v.setPrecision(3)));
        recordsWithFivePrecision.merge(recordsWithThreePrecision)
                .to(OUTPUT_TOPIC, Produced.with(Serdes.String(), getHotelWithWeatherSerde()));
        return builder;
    }

    private KeyValue<String, Weather> getChangedWeather(Weather value, int precision) {
        String geoKey = GeoHash.geoHashStringWithCharacterPrecision(Double.valueOf(value.getLat()),
                Double.valueOf(value.getLng()), precision);
        return new KeyValue<>(geoKey, value);
    }
}
