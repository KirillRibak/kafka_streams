import ch.hsr.geohash.GeoHash;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.logging.log4j.util.Strings;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class Main {

    private static Logger log = LoggerFactory.getLogger(Main.class);
    private static final String READ_TOPIC_PROPERTY = "read_topic";
    private static final String CREATE_HOTELS_WITH_WEATHER_PROPERTY = "task";
    private static final String CUT_GEOHASH = "cut_geohesh";

    private static List<String> OPTIONS = Arrays.asList("read_topic", "task","cut_geohesh");
    public static void main(String[] args) {
        KafkaStreamTask stream = new KafkaStreamTask();
        KafkaStreamUtil utilStream = new KafkaStreamUtil();
        args= new String[1];
        args[0]="task";
        try {
            if (OPTIONS.contains(args[0])) {
                String option = args[0];
                switch (option) {
                    case READ_TOPIC_PROPERTY: {
                        log.info(READ_TOPIC_PROPERTY);
                        if (args[1] != null) {
                            start(utilStream.readDataFromTopic(args[1]), utilStream.getProperties());
                        }
                        break;
                    }
                    case CREATE_HOTELS_WITH_WEATHER_PROPERTY: {
                        log.info(CREATE_HOTELS_WITH_WEATHER_PROPERTY);
                        start(stream.createHotelsWithWeather(), stream.getProperties());
                        break;
                    }
                    case CUT_GEOHASH: {
                        log.info(CUT_GEOHASH);
                        start(utilStream.addGeohashToHotels("hotels"), utilStream.getProperties());
                        break;
                    }
                    default: {
                        log.info("Invalid argument: " + args[0]);
                    }
                }
            } else {
                log.info("Invalid argument: " + args[0]);
            }
        }catch (ArrayIndexOutOfBoundsException e){
            log.info("Expected one or two args");
        }
    }

    public static void start(StreamsBuilder builder, Properties properties) {
        KafkaStreams streams = new KafkaStreams(builder.build(), properties);
        streams.start();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                streams.close();
                log.info("Kafka stream was stopped.");
            } catch (Exception e) {
                log.error("Exception thrown while stream close process", e);
            }
        }));
    }
}

