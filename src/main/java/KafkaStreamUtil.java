import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Produced;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KafkaStreamUtil extends KafkaStream {
    private Logger log = LoggerFactory.getLogger(KafkaStreamUtil.class);

    public StreamsBuilder readDataFromTopic(String topic) {
        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> stream = builder.stream(topic);
        stream.foreach((key, value) -> log.info(key + "=>" + value));
        return builder;
    }

    public StreamsBuilder addGeohashToHotels(String topic) {
        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> hotels = builder.stream(topic);
        KStream<String, String> geohash = hotels.map((key, value) ->
                KeyValue.pair(((String) new JSONObject(value).get("geohash")).substring(0,3), value));
        geohash.to(SMALL_TABLE_TOPIC, Produced.with(Serdes.String(), Serdes.String()));
        return builder;
    }

}
