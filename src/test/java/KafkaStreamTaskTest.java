import model.Hotel;
import model.HotelWithWeather;
import model.Weather;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class KafkaStreamTaskTest {


    private final KafkaStreamTask app = new KafkaStreamTask();

    private Weather weather;
    private Hotel hotel;
    private HotelWithWeather hotelWithWeather;

    void start() {
        weather = new Weather();
        weather.setLat("48.8776756");
        weather.setLng("2.3493159");
        weather.setAvg_tmpr_c("21.2");
        weather.setAvg_tmpr_f("70.2");
        weather.setWthr_date("2017-08-12");
        hotel = new Hotel();
        hotel.setAddress("46 Avenue George V 8th arr 75008 Paris France");
        hotel.setName("H tel Barri re Le Fouquet s");
        hotel.setId("3427383902209");
        hotel.setCountry("FR");
        hotel.setCity("Paris");
        hotel.setLatitude("48.8776756");
        hotel.setLongitude("2.3493159");
        hotel.setGeohash("u09wh");
        hotelWithWeather = new HotelWithWeather(weather, hotel);
    }

    private List<HotelWithWeather> readOutputTopic(TopologyTestDriver testDriver,
                                                   String topic,
                                                   Deserializer<String> keyDeserializer,
                                                   Deserializer<HotelWithWeather> valueDeserializer) {
        List<HotelWithWeather> results = new ArrayList<>();

        while (true) {
            ProducerRecord<String, HotelWithWeather> record =
                    testDriver.readOutput(topic, keyDeserializer, valueDeserializer);
            if (record != null) {
                results.add(record.value());
            } else {
                break;
            }
        }
        return results;
    }

    @Test
    public void testTopology() {
        start();
        Topology topology = app.createHotelsWithWeather().build();
        String tableTopic = KafkaStream.TABLE_TOPIC;
        String smallTableTopic = KafkaStream.SMALL_TABLE_TOPIC;
        String streamTopic = KafkaStream.STREAM_TOPIC;
        String outputTopic = KafkaStream.OUTPUT_TOPIC;
        TopologyTestDriver testDriver = new TopologyTestDriver(topology, app.getProperties());
        Serializer<String> keySerializer = Serdes.String().serializer();
        Serializer<Weather> weatherSerializer = app.weatherSerializer;
        Serializer<Hotel> hotelSerializer = app.hotelSerializer;
        Deserializer<String> stringDeserializer = Serdes.String().deserializer();
        Deserializer<HotelWithWeather> hotelWithWeatherDeserializer = app.hotelWithWeatherDeserializer;

        ConsumerRecordFactory<String, Weather> weatherFactory = new ConsumerRecordFactory<>(keySerializer, weatherSerializer);
        ConsumerRecordFactory<String, Hotel> hotelFactory = new ConsumerRecordFactory<>(keySerializer, hotelSerializer);
        ConsumerRecordFactory<String, Hotel> samllHotelFactory = new ConsumerRecordFactory<>(keySerializer, hotelSerializer);

        List<Hotel> hotels = Arrays.asList(hotel);
        List<Weather> weatherList = Arrays.asList(weather);
        List<HotelWithWeather> hotelWithWeatherList = Arrays.asList(hotelWithWeather);


        for (Hotel hotel : hotels) {
            testDriver.pipeInput(hotelFactory.create(tableTopic, hotel.getGeohash(), hotel));
        }

        for (Hotel hotel : hotels) {
            testDriver.pipeInput(samllHotelFactory.create(smallTableTopic, hotel.getGeohash().substring(0, 3), hotel));
        }

        for (Weather weather : weatherList) {
            testDriver.pipeInput(weatherFactory.create(streamTopic, null, weather));
        }

        List<HotelWithWeather> actualOutput = readOutputTopic(testDriver, outputTopic, stringDeserializer, hotelWithWeatherDeserializer);

        assertEquals(hotelWithWeatherList, actualOutput);
    }
}
